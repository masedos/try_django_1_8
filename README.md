

## Try Django 1.8


## Blog: https://try-django-1-8-masedos.c9users.io/

Tutorial by hhttps://www.youtube.com/watch?v=KsLHt3D_jsE

1) Initialized empty Git repository in ~/try_django_1_8/.git/

    $ git init  

    $ git config user.name "Fernandes Macedo"
    $ git config user.email masedos@egmail.com
    $ git status
    $ git add -A .
    $ git commit -m "first commit"

2) Create a new repository on github try_django_1_8

    $ git remote add origin https://masedos@bitbucket.org/masedos/try_django_1_8.git
    $ git push -u origin master


## Starting from the Terminal

In case you want to run your Django application from the terminal just run:

3) Run migrate command to sync models to database and create Django's default superuser and auth system

    $ python manage.py makemigrations
    $ python manage.py migrate

4) Run Django

    $ python manage.py runserver $IP:$PORT
    
5) Link
    sudo ln -nsf /usr/bin/python3.4  /usr/bin/python 
    