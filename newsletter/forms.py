# -*- coding: utf-8 -*-

from django import forms
from newsletter.models import SignUp

# Create your forms here.


class ContactForm(forms.Form):
    full_name = forms.CharField(label='Yöur Full Name', required=False)
    email = forms.EmailField(label='Your e-mail')
    message = forms.CharField(label='Your message')
    
   
class SignUpForm(forms.ModelForm):
    class Meta:
        model = SignUp
        fields = ['full_name', 'email']
        
    def clean_email(self):
        email = self.cleaned_data.get("email")
        email_base, provider = email.split("@")
        domain, extension = provider.split(".")
        #if not domain == "USC":
        #    raise forms.ValidationError("Please make sure you use your USC email.")
        if not extension == "edu":
            raise forms.ValidationError("Please use a valid .EDU email address.")
        return email
    
    def clean_full_name(self):
        full_name = self.cleaned_data.get("full_name")
        return full_name