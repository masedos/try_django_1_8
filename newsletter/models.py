# -*- coding: utf-8 -*-

from django.db import models
from django.utils.html import format_html

# Create your models here.

class SignUp(models.Model):
    email = models.EmailField()
    full_name = models.CharField(max_length=120, blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    color_code = models.CharField(default="", max_length=6)
   
    def __str__(self):
        return self.email
        
    def colored_full_name(self):
        return format_html('<span style="color: #{};">{}</span>',
                           self.color_code,
                           self.full_name)

    colored_full_name.allow_tags = True
    colored_full_name.admin_order_field = 'full_name'
        
    class Meta:
		verbose_name_plural = 'Sign Up'