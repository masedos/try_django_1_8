# -*- coding: utf-8 -*-

from django.contrib import admin
from newsletter.models import SignUp
from newsletter.forms import SignUpForm

# Register your models here.


class SignUpAdmin(admin.ModelAdmin):
    #search_fields = ['email', 'full_name']
    #fields = ('email', 'full_name', 'color_code')
    #list_display = ('email', 'colored_full_name', 'timestamp', 'updated')
    form = SignUpForm
    
admin.site.register(SignUp, SignUpAdmin)
