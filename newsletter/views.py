# -*- coding: utf-8 -*-

from django.conf import settings
from django.shortcuts import render
from newsletter.forms import SignUpForm, ContactForm
from django.core.mail import send_mail

from django.core.mail import EmailMessage
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from newsletter.models import SignUp

# Create your views here.

def home(request):
    title = "Sign Up Now"
    form = SignUpForm(request.POST or None )

    #if request.user.is_authenticated():
    #    title = "My little title %s" %(request.user)
    #if request.method == "POST":
    #   print request.POST
        
    context = {
        "title":title,
        "form":form,
    }
    if form.is_valid():
        #print  request.POST['email']
        #form.save()
        instance = form.save(commit=False)
        
        full_name = form.cleaned_data.get("full_name")
        if not full_name:
            full_name = "New full name"
        instance.full_name = full_name
        #if not instance.full_name:
        #    instance.full_name = "Macedo"
        instance.save()
        context = {
        "title":"Thank you",
    }
    
    if request.user.is_authenticated() and request.user.is_staff:
        # print (SignUp.objects.all())
        # i = 1
        # for instance in SignUp.objects.all():
        #     print (i)
        #     print (instance.full_name)
        #     i += 1
        queryset = SignUp.objects.all().order_by('-timestamp')
        context = {
            "queryset": queryset,
            }
    
    return render(request, "home.html", context)

def contact(request):
    title = "Contact Us"
    text_align_center = True
    form = ContactForm(request.POST or None)
    if form.is_valid():
        #for key, value in form.cleaned_data.iteritems():
            #print key, value
            #print form.cleaned_data.get(key, value)
        form_email = form.cleaned_data.get("email")
        form_message = form.cleaned_data.get("message")
        form_full_name = form.cleaned_data.get("full_name")
        #print email, message, full_name 
        subject = 'Site contact form'
        from_email = settings.EMAIL_HOST_USER
        to_email = [from_email, 'becarafaela@gmail.com']
        message = " %s: %s via %s "%(
            form_full_name, 
            form_message, 
            form_email)
        some_html_message = """
        <h1>Hello</h1>
            """
        send_mail(
            subject,
            message, 
            from_email, 
            to_email, 
            html_message = some_html_message,
            fail_silently=False)
    
    context = {
        "form": form,
        "title": title,
        "text_align_center":text_align_center,
        }
    return render(request, "forms.html", context)